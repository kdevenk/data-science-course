How to perform Data science
 
Before learning the process of [data science](https://intellipaat.com/blog/what-is-data-science/), let me tell you a story. Once my mother asked me to
buy a sofa set for our new house, being a member of a middle class family, I will definitely go for the best with the cheapest cost. But to optimize my 
purchasing experience, I needed to do a lot of RESAERCH. I list all the good furniture selling websites available, I checked for the ratings of those websites, 
and finally I looked for discounts. Then I made a check list with all my requirements like the desired size, pattern, and other details. Now, after surfing and 
finding the results, I communicated the results to my mother. So we see, that the process included- 
1. Asking the right question and exploring the data. 
2. Modeling the data using various algorithms. 
3. Finally communicating and visualizing the results. 

Now this is what a data scientist do- 
He tries to know what problem he  needs to solve. 
After receiving the input he tries to perform some exploratory analysis for that data. For example, he needs to clean the data to ensure if everything is fine. 
Then he needs to do modeling, for example, if you want to do machine learning, you need to decide which algorithm to use, which model to use, and then he needs 
to train the model. 
And then he runs his data through this model and this process and then he comes out with the final results which includes visualizing the results and finding a 
way to communicate the results to the client. 
Data Science is the top trending technology of the era and is widely used in every sector. Data Science has stretched its roots deep down in the IT world and 
student / professionals are keen to learn Data Science. Data Science has gained vast inference and has extended its roots deep in the IT industry. This 
popularity has made students and professionals to master data science. To curb this, various [Data Science courses](https://intellipaat.com/data-scientist-course-training/)
are available through which one can become Data Scientist with no difficulty. 

Thus we can summarize the life cycle of data science as- 

1) Concept study which is basically understanding the problem, asking the right question and trying to see if there is enough data to solve these problems. 

2) Data Preparation states that the raw data needs to be manipulated to have data in a proper format to be used by the model. 

3) Model Planning is deciding what algoritms, what kind of models will you use. 

4) Model Building is the phase where exact execution of the model takes place. You implement and execute and put the data in analysis  and then you get the 
results. 

5) Communicate results include the packaging, presentation and communication of the result to the stakeholders.

6) Operationalize happens once the data is accepted. 
 
So we see that there is a huge demand of data science and the supply is very low,Industries with high demand of data science are- 

- Gaming
- Healthcare
- Technology
- Marketing
- Finance

Globally there is a huge demand of data science and this a very critical skill that will be required currently and in future. 